<div class="modal fade" id="modal_article"  tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-">
        <div class="modal-content">
            <div class="d-flex justify-content-center align-items-center wating" >
                <i class="fas fa-2x"></i>
            </div>
            <div class="modal-header">
                <h4 class="modal-title">{{__('main.article')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible" id="msg_alert" style="display: none">
                    <p></p>
                </div>
                <div class="card-body" id="">
                    <form id=frm" class="frm">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <img src="{{asset('upload.png')}}" default_img="{{asset('upload.png')}}" style="width: 300px;height: 300px;" class="img-fluid" id="img" alt="upload image">
                                    <input  type="file"  name="file" id="img-file" accept="image/*" style="display:none;">
                                </div>
                            </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="result" class="col-sm-4 col-form-label">{{__('main.title')}}:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="result" class="col-sm-2 col-form-label">{{__('main.description')}}:</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control"  rows="6" id="description" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="modal-footer justify-content-between" id="statusFooter">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('main.close')}}</button>
                <button type="button" class="btn btn-success" id="btn_save"><i class="fas fa-save"></i>&nbsp;{{__('main.save')}}</button>
            </div>
        </div>
    </div>
</div>
