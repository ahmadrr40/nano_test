@extends('layouts.master')
@section("content")
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-olive">
                        <div class="d-flex justify-content-center align-items-center wating" id="overlay">
                            <i class="fas fa-2x"></i>
                        </div>
                        <div class="card-header">
                            <div class="row">
                                <div class="card-title col-md-12">
                                    @if(auth()->user()->can(\App\Enum\PermissionEnum::add_article->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                        <div class="row">
                                            <button class="card-title btn btn-success col-md-12" id="btn_add" data-toggle="modal" data-target="#modal_article">
                                                {{__('main.add')}}&nbsp;&nbsp;<i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="card-body">
                            <table class="table table-bord`ered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('main.title')}}</th>
                                    <th>{{__('main.description')}}</th>
                                    <th>{{__('main.author')}}</th>
                                    <th>{{__('main.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($articles as $article)
                                    <tr>
                                        <td>{{$article->title}}</td>
                                        <td  style="word-break: break-all;">{{$article->description}}</td>
                                        <td>{{$article->author->username}}</td>
                                        <td>
                                            @if(auth()->user()->can(\App\Enum\PermissionEnum::edit_article->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                                <button type="button" id="{{$article->id}}" class="btn btn-primary btn_edit" data-toggle="modal" data-target="#modal_article">
                                                <i class="far fa-edit"></i></button>&nbsp
                                            @endif
                                            @if(auth()->user()->can(\App\Enum\PermissionEnum::delete_article->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                                <button type="button" id="{{$article->id}}" class="btn btn-danger btn_delete">
                                                    <i class="far fa-trash-alt"></i></button>&nbsp
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="show-paginate">
                                Showing {{($articles->currentpage()-1)*$articles->perpage()+1}}
                                to {{$articles->currentpage()*$articles->perpage()}}
                                of {{$articles->total()}} entries
                            </div>
                            {{ $articles->links("pagination::bootstrap-4") }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('articles.add-modal')
@endsection
@section('script')
<script src={{asset("js/article.js")}} type="module"></script>
@endsection


