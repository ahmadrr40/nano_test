@extends('layouts.master')
@section("content")
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-olive">
                        <div class="d-flex justify-content-center align-items-center" id="overlay">
                            <i class="fas fa-2x"></i>
                        </div>
                        <div class="card-header">
                            <div class="row">
                                <div class="card-title col-md-12">
                                    <div class="form-group">
                                        <label for="statusSelect">{{__('main.roles')}}:</label>
                                        <select class="form-control select2" name="user_roles" id ="user_roles">
                                            <option {{($status == null)? 'selected':''}} value="-1">Please Select Role</option>
                                            @foreach(\App\Enum\RoleEnum::asArray() as $role)
                                                <option {{($status == $role)? 'selected':''}} value="{{$role}}">{{$role}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            <div class="row">
                                <div class="card-title col-md-12">
                                    @if(auth()->user()->can(\App\Enum\PermissionEnum::add_user->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                        <div class="row">
                                            <button class="card-title btn btn-success col-md-12" id="btn_add"  data-toggle="modal" data-target="#modal_user">
                                                {{__('main.add')}}&nbsp;&nbsp;<i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="card-body">
                            <table class="table table-bord`ered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('main.first_name')}}</th>
                                    <th>{{__('main.last_name')}}</th>
                                    <th>{{__('main.username')}}</th>
                                    <th>{{__('main.roles')}}</th>
                                    <th>{{__('main.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->roles->first()->name}}</td>
                                        <td>
                                            @if(auth()->user()->can(\App\Enum\PermissionEnum::edit_user->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                                <button type="button" id="{{$user->id}}" class="btn btn-primary btn_edit" data-toggle="modal" data-target="#modal_user">
                                                <i class="far fa-edit"></i></button>&nbsp
                                            @endif
                                            @if(auth()->user()->can(\App\Enum\PermissionEnum::delete_user->value) || auth()->user()->hasRole(\App\Enum\RoleEnum::admin->value))
                                                <button type="button" id="{{$user->id}}" class="btn btn-danger btn_delete">
                                                    <i class="far fa-trash-alt"></i></button>&nbsp
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="show-paginate">
                                Showing {{($users->currentpage()-1)*$users->perpage()+1}}
                                to {{$users->currentpage()*$users->perpage()}}
                                of {{$users->total()}} entries
                            </div>
                            {{ $users->links("pagination::bootstrap-4") }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('users.add-modal')
@endsection
@section('script')
<script src={{asset("js/user.js")}} type="module"></script>
@endsection


