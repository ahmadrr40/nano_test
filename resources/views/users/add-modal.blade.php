<div class="modal fade" id="modal_user"  tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="d-flex justify-content-center align-items-center" id="overlay">
                <i class="fas fa-2x"></i>
            </div>
            <div class="modal-header">
                <h4 class="modal-title">{{__('main.user')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible" id="msg_alert" style="display: none">
                    <p></p>
                </div>
                <div class="card-body" id="">
                    <form id=frm" class="frm">
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="result" class="col-sm-4 col-form-label">{{__('main.first_name')}}:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="result" class="col-sm-4 col-form-label">{{__('main.last_name')}}:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="result" class="col-sm-2 col-form-label">{{__('main.username')}}:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                            </div>
                            <div class="col-6" id="div_password">
                                <label for="result" class="col-sm-2 col-form-label">{{__('main.password')}}:</label>
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('main.roles')}}:</label>
                            <select class="form-control select2" name="role" id ="role">
                                <option {{($status == null)? 'selected':''}} value="-1">Please Select Role</option>
                                @foreach(\App\Enum\RoleEnum::asArray() as $role)
                                    <option {{($status == $role)? 'selected':''}} value="{{$role}}">{{$role}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row" id="div_permission" style="display: none;">
                            <div class="col-12">
                                <label for="result" class="col-sm-2 col-form-label">{{__('main.permissions')}}:</label>
                                    <div class="col-sm-12">
                                        @foreach(\App\Enum\PermissionEnum::editorPermission() as $permission)
                                            <div class="form-check-inline col-2">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input checkbox" name="{{$permission}}" id="{{$permission}}" value="{{$permission}}">{{$permission}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="modal-footer justify-content-between" id="statusFooter">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('main.close')}}</button>
                <button type="button" class="btn btn-success" id="btn_save"><i class="fas fa-save"></i>&nbsp;{{__('main.save')}}</button>
            </div>
        </div>
    </div>
</div>
