</body>
<!-- jQuery -->
<script src={{asset("js/jquery.min.js")}}></script>
<script src={{asset("js/bootstrap.bundle.min.js")}}></script>
<script src={{asset("js/adminlte.min.js")}}></script>
<!-- dataTables -->
<script src={{asset("plugin/datatables/jquery.dataTables.min.js")}}></script>
<script src={{asset("plugin/datatables-bs4/js/dataTables.bootstrap4.min.js")}}></script>
<!-- Select2 -->
<script src={{asset("plugin/select2/js/select2.full.min.js")}}></script>
<!-- Toastr -->
<script src={{asset("plugin/toastr/toastr.min.js")}}></script>
<!--sweetalert2-->
<script src={{asset("plugin/sweetalert2/sweetalert2.all.min.js")}}></script>
<script src={{asset("js/main.js")}}></script>
@yield('script')
</html>
