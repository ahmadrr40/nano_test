<?php

namespace App\Http\Requests;

use App\Enum\PermissionEnum;
use App\Enum\RoleEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'permissions' => explode(',',$this->chek_permission),
        ]);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['sometimes','string','max:100'],
            'last_name' => ['sometimes','string','max:100'],
            'username' => ['sometimes','string','max:100','unique:users,username,'.$this->id],
            'role' => ['sometimes','string',Rule::in(RoleEnum::asArray())],
            'permissions' => Rule::when($this->role == RoleEnum::editor->value,['sometimes','array']),
            'permissions.*' => Rule::when($this->role == RoleEnum::editor->value,['string',Rule::in(PermissionEnum::asArray())]),
        ];
    }
}
