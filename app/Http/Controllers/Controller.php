<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public static function getJsonResponse($messageKey, $data = null, $result = true, $code = Response::HTTP_OK, $exception = null, $replace = []): JsonResponse
    {
        $response = [
            'message' => trans('main.' . Str::slug($messageKey, '_'), $replace, app()->getLocale()),
            'data' => $data,
        ];
        if (env('app_env') != 'production') {
            if ($exception != null) $response['exception'] = $exception;
        }
        return response()->json($response, $code);
    }
}
