<?php

namespace App\Http\Controllers;

use App\Enum\PathEnum;
use App\Http\Requests\AddArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;
use Illuminate\Support\Facades\DB;
use Throwable;

class ArticleController extends Controller
{
    public function index()
    {
        return view('articles.index',['articles' => Article::paginate(10)]);
    }

    public function show(Article $article)
    {
        return self::getJsonResponse('success',['article' => $article->loadMissing(['author'])]);
    }

    public function store(AddArticleRequest $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->validated();
            if(isset($data['img']))
                $data['file_name'] = FileController::storePublicly($request->img,PathEnum::article->value);
            $article = Article::create($data);
            DB::commit();
        } catch (Throwable $exception) {
            !isset($data['file_name'])?:FileController::deletePublicly(PathEnum::article->value.$data['file_name']);
            DB::rollBack();
            throw $exception;
        }
        return self::getJsonResponse('success',$article);
    }

    public function update(UpdateArticleRequest $request,Article $article)
    {
        try{
            DB::beginTransaction();
            $data = $request->validated();
            if($article->file_name && isset($data['img']))
            {
                FileController::deletePublicly(PathEnum::article->value.$article->file_name);
                $data['file_name'] = FileController::storePublicly($data['img'],PathEnum::article->value);
            }
            $article->update($data);
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        return self::getJsonResponse('success',$article);
    }

    public function destroy(Article $article)
    {
        if($article->file_name)
            FileController::deletePublicly(PathEnum::article->value.$article->file_name);
        $article->delete();
        return self::getJsonResponse('success');
    }
}
