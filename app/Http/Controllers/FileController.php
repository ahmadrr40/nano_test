<?php

namespace App\Http\Controllers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public static function storePublicly(UploadedFile $file, string $path, string $name = null): bool|string|null
    {
        if (!$name)
            $name = now()->format('YmdHisu').'_.webp';
        $stored = $file->storePubliclyAs('public' . DIRECTORY_SEPARATOR . $path, $name);
        return $stored ? $name : false;
    }

    public static function deletePublicly(string $path)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . $path;
        if (file_exists(Storage::path($path))) {
            Storage::delete($path);
        }
    }
}
