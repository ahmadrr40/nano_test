<?php

namespace App\Http\Controllers;

use App\Enum\RoleEnum;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Throwable;

class UserController extends Controller
{
    public function index($status=null)
    {
        $users = User::with('roles')->where('id','!=',auth()->user()->id);
        if(isset($status) && in_array($status,RoleEnum::asArray()))
            $users = $users->whereHas('roles',function ($query) use($status){
                return $query->where('name',$status);
            });
        return view('users.index',['status'=>$status,'users' => $users->paginate(10)]);
    }

    public function show(User $user)
    {
        $user = $user->loadMissing(['roles','permissions']);
        return self::getJsonResponse('success',['user' => $user]);
    }

    public function store(AddUserRequest $request)
    {
        try{
            DB::beginTransaction();
            $user = User::create($request->validated());
            if($request->role == RoleEnum::admin->value)
                $user->assignRole(RoleEnum::admin->value);
            else
            {
                $user->assignRole(RoleEnum::editor->value);
                $user->givePermissionTo($request->permissions);
            }
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        return self::getJsonResponse('success', $user);
    }

    public function update(UpdateUserRequest $request,User $user)
    {
        try{
            DB::beginTransaction();
            $user->update($request->validated());
            if($request->role != $user->roles()->first()->name)
                $user->syncRoles([$request->role]);
            $user->syncPermissions($request->permissions);
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        return self::getJsonResponse('success', $user);
    }

    public function destroy(User $user)
    {
        try{
            DB::beginTransaction();
            $user->syncRoles([]);
            $user->syncPermissions([]);
            $user->delete();
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        return self::getJsonResponse('success');
    }

}
