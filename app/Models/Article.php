<?php

namespace App\Models;

use App\Enum\PathEnum;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Article $model){
            $model->user_id =auth()->user()->id;
        });
    }

    protected $fillable = [
        'title',
        'description',
        'file_name',
        'user_id'
    ];

    protected $appends = ['url'];

    public function url(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (!$this->file_name)
                    return null;
                $path = PathEnum::article->value;
                return asset('storage/' . $path . $this->file_name);
            }
        );
    }

    public function author():BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
