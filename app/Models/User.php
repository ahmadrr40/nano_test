<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,HasRoles,HasPermissions;

    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function password(): Attribute
    {
        return Attribute::make(
            set: fn($value) => $this->attributes['password'] = Hash::make($value),
        );
    }

    public function articles():HasMany
    {
        return $this->hasMany(Article::class,'author_id');
    }
}
