<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (Throwable $exception, $request) {
            switch ($exception) {
                case $exception instanceof ValidationException:
                    if ($request->wantsJson()) {
                        return Controller::getJsonResponse('invalid_data', $exception->errors(), false, Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    return back()->with($exception->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
                case $exception instanceof AuthenticationException:
                    if ($request->wantsJson()) {
                        return Controller::getJsonResponse('unauthenticated', [], false, Response::HTTP_UNAUTHORIZED);
                    }
                    return response()->view('login');
                case $exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException
                    or $exception instanceof AccessDeniedHttpException:
                    if ($request->wantsJson()) {
                        return Controller::getJsonResponse('unauthorized', [], false, Response::HTTP_FORBIDDEN);
                    }
                    return response()->view('errors.403');
                case $exception instanceof RecordsNotFoundException
                    or $exception instanceof NotFoundHttpException
                    or $exception instanceof FileNotFoundException
                    or $exception instanceof RouteNotFoundException:
                    if ($request->wantsJson()) {
                        return Controller::getJsonResponse($exception->getMessage(), [], false, Response::HTTP_NOT_FOUND);
                    }
                    return view('errors.404');
                default:
                    if ($request->wantsJson()) {
                        return Controller::getJsonResponse('internal_server_error', $exception->getMessage(), false, Response::HTTP_INTERNAL_SERVER_ERROR, $exception->getTraceAsString());
                    }
                    return response()->view('errors.500');
            }
        });
    }
}
