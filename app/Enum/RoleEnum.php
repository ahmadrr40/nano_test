<?php

namespace App\Enum;

enum RoleEnum:string
{
    case admin = "admin";
    case editor ="editor";

    public static function asArray(): array
    {
        return array_map(fn($x) => $x->value, self::cases());
    }
}
