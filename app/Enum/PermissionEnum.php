<?php

namespace App\Enum;

enum PermissionEnum:string
{
    case view_user ="view_users";
    case add_user = "add_user";
    case edit_user ="edit_user";
    case delete_user ="delete_user";

    case view_article ="view_articles";
    case add_article = "add_article";
    case edit_article ="edit_article";
    case delete_article ="delete_article";

    public static function asArray(): array
    {
        return array_map(fn($x) => $x->value, self::cases());
    }

    public static function editorPermission(): array
    {
        return [self::view_user->value,self::view_article->value,self::add_article->value,self::edit_article,self::delete_article->value];
    }
}
