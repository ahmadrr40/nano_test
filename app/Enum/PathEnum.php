<?php

namespace App\Enum;

enum PathEnum:string
{
    case article = "article/";

    public static function asArray(): array
    {
        return array_map(fn($x) => $x->value, self::cases());
    }
}
