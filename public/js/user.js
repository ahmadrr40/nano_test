import {BaseAjaxClass} from './BaseAjaxClass.js';
import {enums} from './enums.js';
$(function(){
    var _url =  $("#base_url").val()+"/cms/users/",
        _role_select = $('#user_roles'),
        _id=0,
        is_update = false;

    _role_select.change(function (){
       window.location.href = _url+this.value;
    });

    $('.checkbox').not('#view_articles').click(function() {
        $('#view_articles').prop("checked",($('#edit_article').is(":checked") && $('#delete_article').is(":checked")));
    });

    $('#role').change(function(){
       if(this.value == "editor")
           $('#div_permission').css('display','');
       else
       {
           $('#div_permission').css('display','none');
           $('.checkbox').prop('checked',false);
       }

    });

    $('#btn_add').click(function (){
        $('#div_password').css('display','');
        clear();
    });


    $(document).on("click","#btn_save",function() {
        var permissions = [];
        $(".checkbox:checked").each(function() {
            permissions.push($(this).val());
        });
        var data = $('.frm').serialize()+"&chek_permission="+permissions+"&id="+_id;
        var url = (is_update)?_url+"update/"+_id :_url;
        BaseAjaxClass.sendAjax(url,data,enums.my_method.post);
    });

    $(document).on("click",".btn_edit",function() {
        clear();
        $('#div_password').css('display','none');
        is_update = true;
        _id = this.id;
        BaseAjaxClass.sendAjax(_url+"info/"+_id ,[],enums.my_method.get);
         if(BaseAjaxClass.return_data.hasOwnProperty('user'))
         {
             var user = BaseAjaxClass.return_data.user;
             $('#first_name').val(user.first_name);
             $('#last_name').val(user.last_name);
             $('#username').val(user.username);
             $('#role').val(user.roles[0].name).trigger('change');
             if(user.roles[0].name == enums.user_type.editor)
             {
                 for(var i=0;i<user.permissions.length;i++)
                 {
                     $(".checkbox").each(function() {
                         if($(this).val() == user.permissions[i].name)
                             $(this).prop('checked',true);
                     });
                 }
             }
         }
    });

    $(document).on("click",".btn_delete",function() {
        Swal.fire({
            title: "Please confirm delete action",
            icon: "question",
            showCancelButton: true,
            showButtonPanel: true,
            showCloseButton: true,
            focusConfirm: false,
        }).then((result) => {
            if (result.isConfirmed) {
                BaseAjaxClass.sendAjax(_url+this.id ,[],enums.my_method.delete);
                if(BaseAjaxClass.status == 200)
                {
                    var row = $(this).parent().parent();
                    row.remove();
                }

            }
            else return;
        });
    });

    function clear()
    {
        BaseAjaxClass.clearErrorMsg()
        $('.checkbox').prop('checked',false);
        $('#role').val(-1).trigger('change');
        $('.form-control').val('');
    }
});
