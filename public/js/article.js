import {BaseAjaxClass} from './BaseAjaxClass.js';
import {enums} from './enums.js';
$(function(){
var _url =  $("#base_url").val()+"/cms/articles/",
    _id=0,
    is_update = false,
    _img   = $('#img'),
    _imgFile = $("#img-file"),
    default_img = _img.attr('default_img');

    _img.click(function(){_imgFile.click();});
    _imgFile.change(function(file){_img.attr('src',window.URL.createObjectURL(_imgFile[0].files[0]));});
    $('#btn_add').click(function (){clear();});

    $(document).on("click","#btn_save",function() {
        var frm = new FormData();
        if(_imgFile.val())
        frm.append('img',_imgFile[0].files[0]);
        frm.append('title',$('#title').val());
        frm.append('description',$('#description').val());
        var url = (is_update)?_url+"update/"+_id :_url;
        BaseAjaxClass.sendAjaxWithFile(url,frm,enums.my_method.post);
    });

    $(document).on("click",".btn_edit",function() {
        clear();
        is_update = true;
        _id = this.id;
        BaseAjaxClass.sendAjax(_url+"info/"+_id ,[],enums.my_method.get);
        if(BaseAjaxClass.return_data.hasOwnProperty('article'))
        {
            var article = BaseAjaxClass.return_data.article;
            $('#title').val(article.title);
            $('#description').val(article.description);
            if(article.url != null)
                _img.attr('src',article.url);
        }
    });


    $(document).on("click",".btn_delete",function() {
        Swal.fire({
            title: "Please confirm delete action",
            icon: "question",
            showCancelButton: true,
            showButtonPanel: true,
            showCloseButton: true,
            focusConfirm: false,
        }).then((result) => {
            if (result.isConfirmed) {
                BaseAjaxClass.sendAjax(_url+this.id ,[],enums.my_method.delete);
                if(BaseAjaxClass.status == 200)
                {
                    var row = $(this).parent().parent();
                    row.remove();
                }
            } else return;
        });
    });

    function clear()
    {
        BaseAjaxClass.clearErrorMsg()
        $('.form-control').val('');
        _img.attr('src',default_img);
    }
});
