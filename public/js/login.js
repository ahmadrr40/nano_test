import {BaseAjaxClass} from './BaseAjaxClass.js';
import {enums} from './enums.js';
$(function(){
    var _url = $("#base_url").val();
    $(document).on("click","#btn_signin",function() {
        var data = "username="+$("#username").val()+"&password="+$("#password").val();
        BaseAjaxClass.sendAjax(_url+"/login",data,enums.my_method.post);
    });
});
