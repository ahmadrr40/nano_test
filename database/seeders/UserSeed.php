<?php

namespace Database\Seeders;

use App\Enum\PermissionEnum;
use App\Enum\RoleEnum;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::create([
            "first_name" => "Ahmad",
            "last_name" => "Ahmad",
            'username' => 'admin',
            'password' => 'admin@123'
        ]);
        $admin->assignRole(RoleEnum::admin->value);
        $admin->givePermissionTo(PermissionEnum::asArray());
        ##################################################
        $editor = User::create([
            "first_name" => "Leen",
            "last_name" => "Leen",
            'username' => 'editor',
            'password' => 'editor@123'
        ]);
        $editor->assignRole(RoleEnum::editor->value);
        $editor->givePermissionTo(PermissionEnum::editorPermission());
    }
}
