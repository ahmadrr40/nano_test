<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('', function () {
    if(auth()->check())
        return view('index');
    return view('login');
})->name('login');

Route::post('/login',[AuthController::class,'login']);

Route::group(['prefix'=>'cms/','middleware' => 'auth'],function(){

    Route::get('/signout',function (){
        auth()->logout();
        return redirect('/');
    });

    Route::get('',function (){return view('index');});
    Route::group(['prefix'=>'users/'],function(){
        Route::get('{status?}',[UserController::class,'index'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::view_user->value);
        Route::get('info/{user}',[UserController::class,'show'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::edit_user->value);
        Route::post('',[UserController::class,'store'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::add_user->value);
        Route::post('update/{user}',[UserController::class,'update'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::edit_user->value);
        Route::delete('{user}',[UserController::class,'destroy'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::delete_article->value);
    });

    Route::group(['prefix'=>'articles/'],function(){
        Route::get('',[ArticleController::class,'index'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::view_article->value);
        Route::get('info/{article}',[ArticleController::class,'show'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::edit_article->value);
        Route::post('',[ArticleController::class,'store'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::add_article->value);
        Route::post('update/{article}',[ArticleController::class,'update'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::edit_article->value);
        Route::delete('{article}',[ArticleController::class,'destroy'])->middleware('role_or_permission:'.\App\Enum\RoleEnum::admin->value.'|'.\App\Enum\PermissionEnum::delete_article->value);
    });
});

