-run php artisan key:g
-run composer i
-create database nano_test
-run php artisan migrate fresh --seed
-run php artisan storage:link
-go to env file and set APP_URL=http://localhost:8000
-admin account:
username=>admin
password=>admin@123
-editor account:
username=>editor
password=>editor@123

-gitlab:https://gitlab.com/ahmadrr40/nano_test
